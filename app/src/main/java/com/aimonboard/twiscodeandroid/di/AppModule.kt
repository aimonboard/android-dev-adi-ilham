package com.aimonboard.twiscodeandroid.di

import android.content.Context
import com.aimonboard.twiscodeandroid.data.local.AppDao
import com.aimonboard.twiscodeandroid.data.local.AppDatabase
import com.aimonboard.twiscodeandroid.network.ApiClient
import com.aimonboard.twiscodeandroid.network.ApiInterface
import com.aimonboard.twiscodeandroid.ui.cart.CartRepository
import com.aimonboard.twiscodeandroid.ui.home.HomeRepository
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import retrofit2.Retrofit
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object AppModule {

    // ============================================================================================= Retrofit
    @Singleton
    @Provides
    fun provideApiClient() : Retrofit = ApiClient().buildRetrofit()

    @Singleton
    @Provides
    fun provideApiService(retrofit: Retrofit): ApiInterface = retrofit.create(ApiInterface::class.java)


    // ============================================================================================= Database
    @Singleton
    @Provides
    fun provideDatabase(@ApplicationContext appContext: Context) = AppDatabase.getDatabase(appContext)

    @Provides
    fun provideDatabaseDao(db: AppDatabase) = db.dao()

    // ============================================================================================= Repository
    @Singleton
    @Provides
    fun homeRepository(remoteDataSource: ApiInterface, localDataSource: AppDao) =
        HomeRepository(remoteDataSource, localDataSource)

    @Singleton
    @Provides
    fun cartRepository(localDataSource: AppDao) = CartRepository(localDataSource)


}