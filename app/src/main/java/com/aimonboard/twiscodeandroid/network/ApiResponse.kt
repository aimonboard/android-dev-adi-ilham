package com.aimonboard.twiscodeandroid.network

data class ApiResponse <out T>(val status: ApiStatus, val data: T?, val message: String?) {
    enum class ApiStatus {
        SUCCESS, ERROR
    }

    companion object {
        fun <T> success(data: T?): ApiResponse<T> {
            return ApiResponse(ApiStatus.SUCCESS, data, null)
        }

        fun <T> error(msg: String, data: T?): ApiResponse<T> {
            return ApiResponse(ApiStatus.ERROR, data, msg)
        }
    }
}