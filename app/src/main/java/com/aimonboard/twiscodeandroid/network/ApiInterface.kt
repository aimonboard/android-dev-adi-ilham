package com.aimonboard.twiscodeandroid.network

import com.aimonboard.twiscodeandroid.data.remote.ProductResponse
import io.reactivex.Observable
import retrofit2.http.POST

interface ApiInterface {

    @POST("rest/items/search/api_key/teampsisthebest")
    fun getList(): Observable<ProductResponse>

}