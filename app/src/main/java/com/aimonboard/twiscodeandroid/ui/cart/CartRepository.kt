package com.aimonboard.twiscodeandroid.ui.cart

import androidx.lifecycle.LiveData
import com.aimonboard.twiscodeandroid.data.local.AppDao
import com.aimonboard.twiscodeandroid.data.local.entity.CartEntity
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers.IO
import kotlinx.coroutines.launch
import javax.inject.Inject

class CartRepository @Inject constructor(
    private val localDataSource: AppDao
) {

    fun getCart(): LiveData<List<CartEntity>> {
        return localDataSource.cartGetAll()
    }

    fun plus(productId: String) {
        CoroutineScope(IO).launch {
            localDataSource.cartPlus(productId)
        }
    }

    fun minus(productId: String) {
        CoroutineScope(IO).launch {
            localDataSource.cartMinus(productId)
        }
    }

    fun total(): LiveData<Double> {
        return localDataSource.cartTotal()
    }

}