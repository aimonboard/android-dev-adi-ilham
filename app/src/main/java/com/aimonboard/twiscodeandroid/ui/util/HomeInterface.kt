package com.aimonboard.twiscodeandroid.ui.util

import com.aimonboard.twiscodeandroid.data.local.entity.ProductEntity

interface HomeInterface {
    fun onProductClick(product: ProductEntity)
    fun onCategoryClick(categoryId: String)
}