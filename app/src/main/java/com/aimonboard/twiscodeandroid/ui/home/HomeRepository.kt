package com.aimonboard.twiscodeandroid.ui.home

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.sqlite.db.SimpleSQLiteQuery
import com.aimonboard.twiscodeandroid.data.local.AppDao
import com.aimonboard.twiscodeandroid.data.local.entity.CartEntity
import com.aimonboard.twiscodeandroid.data.local.entity.CategoryEntity
import com.aimonboard.twiscodeandroid.data.local.entity.ProductEntity
import com.aimonboard.twiscodeandroid.data.remote.ProductResponse
import com.aimonboard.twiscodeandroid.network.ApiInterface
import com.aimonboard.twiscodeandroid.network.ApiResponse
import io.reactivex.Completable
import io.reactivex.schedulers.Schedulers
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.functions.Action
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers.IO
import kotlinx.coroutines.launch
//import kotlinx.coroutines.CoroutineScope
//import kotlinx.coroutines.Dispatchers.IO
//import kotlinx.coroutines.GlobalScope
//import kotlinx.coroutines.launch
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.lang.Exception
import javax.inject.Inject

class HomeRepository @Inject constructor(
    private val remoteDataSource: ApiInterface,
    private val localDataSource: AppDao
) {

    fun getProductLocal(
        searchText: String,
        categoryId: String,
        priceOrderBy: String
    ): LiveData<List<ProductEntity>> {
        Log.e("aim", "query param : $searchText, $categoryId, $priceOrderBy")
        val query = SimpleSQLiteQuery(
            "SELECT * FROM product " +
                    "WHERE title LIKE '%$searchText%' " +
                    "AND categoryId LIKE '%$categoryId%' " +
                    "ORDER BY price $priceOrderBy"
        )

//        return localDataSource.productGetAll(searchText, categoryId, priceOrderBy)
        return localDataSource.productGetAll(query)
    }

    fun getCategory(): LiveData<List<CategoryEntity>> {
        return localDataSource.categoryGetAll()
    }

    fun updateCategoryChecked(categoryId: String) {
        Log.e("aim","category Id : $categoryId")
        Completable.fromAction {
            localDataSource.categoryUpdateCheckedAll(false)
            localDataSource.categoryUpdateChecked(categoryId, true)
        }.subscribe()

//        CoroutineScope(IO).launch {
//            localDataSource.categoryUpdateCheckedAll(false)
//            localDataSource.categoryUpdateChecked(categoryId, true)
//        }

    }

    fun getProductRemote() : LiveData<ApiResponse<ProductResponse>> {
        val compositeDisposable = CompositeDisposable()

        val responseData = MutableLiveData<ApiResponse<ProductResponse>>()
        val disposeable = remoteDataSource.getList()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(
                {
                    Log.e("aim", it.toString())
                    responseData.postValue(
                        ApiResponse.success(it)
                    )
                },
                {
                    responseData.postValue(
                        ApiResponse.error(it.toString(),null)
                    )
                }
            )

        compositeDisposable.add(disposeable)

//        remoteDataSource.getList().enqueue(object: Callback<ProductResponse> {
//            override fun onFailure(call: Call<ProductResponse>, t: Throwable) {
//                responseData.value = ApiResponse.error(
//                    t.message.toString(),
//                    null
//                )
//            }
//            override fun onResponse(call: Call<ProductResponse>, response: Response<ProductResponse>) {
//                if (response.isSuccessful) {
//                    responseData.value = ApiResponse.success(response.body())
//                }
//            }
//        })

        return responseData
    }

    fun insertProduct(productData: ArrayList<ProductEntity>) {
        CoroutineScope(IO).launch {
            localDataSource.productInsert(productData)
        }
    }

    fun insertCart(product: ProductEntity) {
        CoroutineScope(IO).launch {
            val checkProduct = localDataSource.cartGetByProductId(product.id)
            if (checkProduct.isEmpty()) {
                localDataSource.cartInsert(
                    CartEntity(
                        id = 0,
                        productId = product.id,
                        title = product.title,
                        price = product.price,
                        city = product.city,
                        count = 1,
                        conditionOfItem = product.conditionOfItem,
                        userName = product.userName,
                        weight = product.weight,
                        isSoldOut = product.isSoldOut,
                        isHalal = product.isHalal
                    )
                )
            } else {
                localDataSource.cartPlus(product.id)
            }
        }
    }

    fun countCart(): LiveData<Int> {
        return localDataSource.cartCount()
    }

    fun insertCategory(categoryData: ArrayList<CategoryEntity>) {
//        Completable.fromAction {
//            localDataSource.categoryInsert(categoryData)
//        }.subscribe()

        CoroutineScope(IO).launch {
            localDataSource.categoryInsert(categoryData)
        }

    }

}