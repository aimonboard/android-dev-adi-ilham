package com.aimonboard.twiscodeandroid.ui.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.aimonboard.twiscodeandroid.data.local.entity.CategoryEntity
import com.aimonboard.twiscodeandroid.databinding.ItemCategoryBinding
import com.aimonboard.twiscodeandroid.ui.util.HomeInterface
import java.util.*
import kotlin.collections.ArrayList

class CategoryAdapter (private val listener: HomeInterface) : RecyclerView.Adapter<CategoryAdapter.ViewHolder>() {

    private var items = mutableListOf<CategoryEntity>()

    fun setItems(items: ArrayList<CategoryEntity>) {
        this.items = items
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding = ItemCategoryBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return ViewHolder(binding, listener)
    }

    override fun getItemCount(): Int = items.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) = holder.bind(items[position])

    inner class ViewHolder(private val itemBinding: ItemCategoryBinding, private val listener: HomeInterface) : RecyclerView.ViewHolder(itemBinding.root) {

        fun bind(item: CategoryEntity) {
            itemBinding.txTitle.isChecked = item.isChecked
            itemBinding.txTitle.text = item.name

            itemBinding.txTitle.setOnClickListener {
                listener.onCategoryClick(item.id)
            }

        }
    }

}