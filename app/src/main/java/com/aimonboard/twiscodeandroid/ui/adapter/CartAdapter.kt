package com.aimonboard.twiscodeandroid.ui.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.recyclerview.widget.RecyclerView
import com.aimonboard.twiscodeandroid.data.local.entity.CartEntity
import com.aimonboard.twiscodeandroid.databinding.ItemCartBinding
import java.text.NumberFormat
import java.util.*
import kotlin.collections.ArrayList

class CartAdapter (private val listener: ItemClickInterface) : RecyclerView.Adapter<CartAdapter.ViewHolder>() {

    private var items = mutableListOf<CartEntity>()

    fun setItems(items: ArrayList<CartEntity>) {
        this.items = items
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding = ItemCartBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return ViewHolder(binding, listener)
    }

    override fun getItemCount(): Int = items.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) = holder.bind(items[position])

    inner class ViewHolder(private val itemBinding: ItemCartBinding, private val listener: ItemClickInterface) : RecyclerView.ViewHolder(itemBinding.root) {

        fun bind(item: CartEntity) {
            val localeID = Locale("in", "ID")
            val formatRupiah = NumberFormat.getCurrencyInstance(localeID)
            val priceString = "Rp ${formatRupiah.format(item.price).replace("Rp", "")}"

            itemBinding.txTitle.text = item.title
            itemBinding.txPrice.text = priceString
            itemBinding.txCondition.text = "(${item.conditionOfItem})"
            itemBinding.txWeight.text = "${item.weight} Kg"
            itemBinding.txCount.text = item.count.toString()

            itemBinding.btnPlus.setOnClickListener {
                listener.plus(item.productId)
            }

            itemBinding.btnMinus.setOnClickListener {
                val count = itemBinding.txCount.text.toString().toInt()
                if (count > 0 ) {
                    listener.minus(item.productId)
                }
            }

        }
    }

    interface ItemClickInterface {
        fun plus(productId: String)
        fun minus(productId: String)
    }

}