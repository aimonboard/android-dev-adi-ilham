package com.aimonboard.twiscodeandroid.ui.home

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.aimonboard.twiscodeandroid.data.local.entity.CategoryEntity
import com.aimonboard.twiscodeandroid.data.local.entity.ProductEntity
import com.aimonboard.twiscodeandroid.data.remote.ProductResponse
import com.aimonboard.twiscodeandroid.network.ApiResponse
import com.google.gson.Gson
import dagger.hilt.android.lifecycle.HiltViewModel
import org.json.JSONObject
import javax.inject.Inject

@HiltViewModel
class HomeViewModel @Inject constructor(
    private val repository: HomeRepository
) : ViewModel() {

    val searchText = MutableLiveData("")
    val categoryId = MutableLiveData("")
    val priceOrderBy = MutableLiveData("ASC")

    fun getProductLocal(): LiveData<List<ProductEntity>> {
        return repository.getProductLocal(
            searchText.value ?: "",
            categoryId.value ?: "",
            priceOrderBy.value ?: ""
        )
    }

    fun getProductRemote(): LiveData<ApiResponse<ProductResponse>> {
        return repository.getProductRemote()
    }

    fun getCategory(): LiveData<List<CategoryEntity>> {
        return repository.getCategory()
    }

    fun updateCategoryChecked() {
        repository.updateCategoryChecked(categoryId.value ?: "")
    }

    fun insertProductAndCategory(response: ProductResponse) {
        val productData = ArrayList<ProductEntity>()
        val categoryData = ArrayList<CategoryEntity>()

        response.forEach {
            productData.add(ProductEntity(
                id = it.id,
                title = it.title,
                price = if (it.price.isNotEmpty()) it.price.toDouble() else 0.0,
                city = handleAnyData(it.getAddress, "city"),
                conditionOfItem = handleAnyData(it.conditionOfItem, "name"),
                categoryId = it.catId,
                userName = it.addedUserName,
                weight = if (it.weight.isNotEmpty()) it.weight.toDouble() else 0.0,
                isSoldOut = if (it.isSoldOut.isNotEmpty()) it.isSoldOut.toInt() else 0,
                isHalal = if (it.isHalal.isNotEmpty()) it.isHalal.toInt() else 0,
            ))

            categoryData.add(CategoryEntity(
                id = it.category.catId,
                name = it.category.catName,
                isChecked = false
            ))
        }

        repository.insertProduct(productData)
        repository.insertCategory(categoryData)
    }

    fun insertCart(product: ProductEntity) {
        repository.insertCart(product)
    }

    fun cartCount(): LiveData<Int> {
        return repository.countCart()
    }

    private fun handleAnyData (data: Any, param: String): String {
        return if (data is String) { "" }
        else {
            val jsonString = Gson().toJson(data)
            val jsonObject = JSONObject(jsonString)
            jsonObject.getString(param)
        }
    }

}