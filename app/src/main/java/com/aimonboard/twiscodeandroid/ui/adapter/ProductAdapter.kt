package com.aimonboard.twiscodeandroid.ui.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.aimonboard.twiscodeandroid.data.local.entity.ProductEntity
import com.aimonboard.twiscodeandroid.databinding.ItemProductBinding
import com.aimonboard.twiscodeandroid.ui.util.HomeInterface
import java.text.NumberFormat
import java.util.*
import kotlin.collections.ArrayList

class ProductAdapter (private val listener: HomeInterface) : RecyclerView.Adapter<ProductAdapter.ViewHolder>() {

    private var items = mutableListOf<ProductEntity>()

    fun setItems(items: ArrayList<ProductEntity>) {
        this.items = items
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding = ItemProductBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return ViewHolder(binding, listener)
    }

    override fun getItemCount(): Int = items.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) = holder.bind(items[position])

    inner class ViewHolder(private val itemBinding: ItemProductBinding, private val listener: HomeInterface) : RecyclerView.ViewHolder(itemBinding.root) {

        fun bind(item: ProductEntity) {
            val localeID = Locale("in", "ID")
            val formatRupiah = NumberFormat.getCurrencyInstance(localeID)
            val priceString = "Rp ${formatRupiah.format(item.price).replace("Rp", "")}"

            itemBinding.txTitle.text = item.title
            itemBinding.txPrice.text = priceString
            itemBinding.tcCity.text = item.city
            itemBinding.txUsername.text = item.userName

            itemBinding.halal.visibility = if (item.isHalal == 1) {
                View.VISIBLE
            } else {
                View.GONE
            }

            itemBinding.txStock.visibility = if (item.isSoldOut == 1) {
                View.GONE
            } else {
                View.VISIBLE
            }

            itemBinding.root.setOnClickListener {
                listener.onProductClick(item)
            }

        }
    }
}