package com.aimonboard.twiscodeandroid.ui.cart

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import com.aimonboard.twiscodeandroid.data.local.entity.CartEntity
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class CartViewModel @Inject constructor(
    private val repository: CartRepository
) : ViewModel() {

    fun getCart(): LiveData<List<CartEntity>> {
        return repository.getCart()
    }

    fun plus(productId: String) {
        repository.plus(productId)
    }

    fun minus(productId: String) {
        repository.minus(productId)
    }

    fun total(): LiveData<Double> {
        return repository.total()
    }

}