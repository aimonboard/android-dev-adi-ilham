package com.aimonboard.twiscodeandroid.ui.home

import android.os.Bundle
import android.view.*
import android.widget.RadioButton
import android.widget.RadioGroup
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.widget.SearchView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.aimonboard.twiscodeandroid.R
import com.aimonboard.twiscodeandroid.data.local.entity.CategoryEntity
import com.aimonboard.twiscodeandroid.data.local.entity.ProductEntity
import com.aimonboard.twiscodeandroid.databinding.FragmentHomeBinding
import com.aimonboard.twiscodeandroid.network.ApiResponse
import com.aimonboard.twiscodeandroid.ui.adapter.CategoryAdapter
import com.aimonboard.twiscodeandroid.ui.adapter.ProductAdapter
import com.aimonboard.twiscodeandroid.ui.util.HomeInterface
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.bottomsheet.BottomSheetDialog
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class HomeFragment : Fragment(), HomeInterface {

    private lateinit var binding: FragmentHomeBinding
    private lateinit var adapter: ProductAdapter
    private lateinit var counterView: TextView
    private lateinit var dialog: BottomSheetDialog
    private val viewModel: HomeViewModel by viewModels()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        setHasOptionsMenu(true)
        binding = FragmentHomeBinding.inflate(inflater, container, false)
        binding.viewModel = viewModel
        binding.lifecycleOwner = this
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        dialog = BottomSheetDialog(requireContext())

        clickListener()
        setupAdapter()
        scrollListener()
        getProductLocal()
        getProductRemote()
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.home_menu, menu)

        val view = menu.findItem(R.id.action_cart).actionView as ConstraintLayout
        view.setOnClickListener {
            findNavController().navigate(R.id.navigation_cart)
        }
        counterView = view.findViewById(R.id.cart_badge)
        getCartCount()


        val searchMenu = menu.findItem(R.id.action_search)
        searchMenu.setOnActionExpandListener(object : MenuItem.OnActionExpandListener {
            override fun onMenuItemActionExpand(item: MenuItem?): Boolean {
                val searchView = searchMenu.actionView as SearchView
                searchView.queryHint = "Search Product"
                searchView.setQuery(viewModel.searchText.value.toString(),false)
                return true
            }

            override fun onMenuItemActionCollapse(item: MenuItem?): Boolean {
                viewModel.searchText.value = ""
                getProductLocal()
                return true
            }
        })

        val searchView = searchMenu.actionView as SearchView
        searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String): Boolean {
                if (!searchView.isIconified) {
                    searchView.isIconified = true
                }
                searchView.setQuery(query, false)
                //searchMenu.collapseActionView()

                viewModel.searchText.value = query
                getProductLocal()
                return false
            }

            override fun onQueryTextChange(s: String?): Boolean {
                return false
            }
        })
    }

    override fun onProductClick(product: ProductEntity) {
        viewModel.insertCart(product)
        Toast.makeText(
            requireContext(),
            "${product.title} added to cart",
            Toast.LENGTH_LONG
        ).show()
    }

    override fun onCategoryClick(categoryId: String) {
        viewModel.categoryId.value = categoryId
        viewModel.updateCategoryChecked()
    }

    private fun clickListener() {
        binding.refreshLayout.setOnRefreshListener {
            getProductRemote()
        }

        binding.btnCategory.setOnClickListener {
            dialogCategory()
        }

        binding.btnFilter.setOnClickListener {
            dialogFilter()
        }
    }

    private fun setupAdapter() {
        adapter = ProductAdapter(this)
        binding.recyclerProduct.adapter = adapter
        binding.recyclerProduct.layoutManager = GridLayoutManager(requireContext(), 2)
    }

    private fun scrollListener() {
        binding.recyclerProduct.setOnScrollChangeListener { v, scrollX, scrollY, oldScrollX, oldScrollY ->
            if (scrollY > oldScrollY) {
                binding.optionLay.visibility = View.GONE
            } else if (scrollY < oldScrollY) {
                binding.optionLay.visibility = View.VISIBLE
            }
        }
    }

    private fun getProductLocal() {
        binding.refreshLayout.isRefreshing = true
        binding.shimmerLayout.startShimmer()
        binding.shimmerLayout.visibility = View.VISIBLE
        binding.recyclerProduct.visibility = View.GONE

        viewModel.getProductLocal().observe(viewLifecycleOwner, {
            adapter.setItems(it as ArrayList<ProductEntity>)

            binding.refreshLayout.isRefreshing = false
            binding.shimmerLayout.stopShimmer()
            binding.shimmerLayout.visibility = View.GONE
            binding.recyclerProduct.visibility = View.VISIBLE
        })
    }

    private fun getProductRemote() {
        binding.refreshLayout.isRefreshing = true
        binding.shimmerLayout.startShimmer()
        binding.shimmerLayout.visibility = View.VISIBLE
        binding.recyclerProduct.visibility = View.GONE

        viewModel.getProductRemote().observe(viewLifecycleOwner, {
            binding.refreshLayout.isRefreshing = false
            binding.shimmerLayout.stopShimmer()
            binding.shimmerLayout.visibility = View.GONE
            binding.recyclerProduct.visibility = View.VISIBLE

            when (it.status) {
                ApiResponse.ApiStatus.SUCCESS -> {
                    if (!it.data.isNullOrEmpty()) {
                        viewModel.insertProductAndCategory(it.data)
                    }
                }
                ApiResponse.ApiStatus.ERROR -> {
                    Toast.makeText(
                        requireContext(),
                        it.message,
                        Toast.LENGTH_LONG
                    ).show()
                }
            }
        })
    }

    private fun getCartCount() {
        viewModel.cartCount().observe(viewLifecycleOwner, {
            if (it > 0) {
                counterView.visibility = View.VISIBLE
                counterView.text = it.toString()
            } else {
                counterView.visibility = View.GONE
            }
        })
    }

    private fun dialogCategory() {
        val view = layoutInflater.inflate(R.layout.dialog_category, null)
        dialog.setContentView(view)
        val bottomSheetBehavior = BottomSheetBehavior.from<View>(view.parent as View)
        dialog.setOnShowListener { bottomSheetBehavior.state = BottomSheetBehavior.STATE_EXPANDED }

        val recyclerCategory = dialog.findViewById<RecyclerView>(R.id.recycler_category)
        val btnApply = dialog.findViewById<TextView>(R.id.btn_apply)
        val btnReset = dialog.findViewById<TextView>(R.id.btn_reset)
        val categoryAdapter = CategoryAdapter(this)
        recyclerCategory?.adapter = categoryAdapter
        recyclerCategory?.layoutManager = LinearLayoutManager(requireContext())

        viewModel.getCategory().observe(viewLifecycleOwner, {
            categoryAdapter.setItems(it as ArrayList<CategoryEntity>)
        })

        btnApply?.setOnClickListener {
            getProductLocal()
            dialog.dismiss()
        }

        btnReset?.setOnClickListener {
            viewModel.categoryId.value = ""
            viewModel.updateCategoryChecked()
            getProductLocal()
            dialog.dismiss()
        }

        dialog.show()
    }

    private fun dialogFilter() {
        val view = layoutInflater.inflate(R.layout.dialog_filter, null)
        dialog.setContentView(view)
        val bottomSheetBehavior = BottomSheetBehavior.from<View>(view.parent as View)
        dialog.setOnShowListener { bottomSheetBehavior.state = BottomSheetBehavior.STATE_EXPANDED }

        val radioGroup = dialog.findViewById<RadioGroup>(R.id.radio_group)
        val radioAsc = dialog.findViewById<RadioButton>(R.id.radio_asc)
        val radioDesc = dialog.findViewById<RadioButton>(R.id.radio_desc)
        val btnSet = dialog.findViewById<TextView>(R.id.btn_set)

        val filterHistory = viewModel.priceOrderBy.value ?: ""
        if (filterHistory.isNotEmpty()) {
            if (filterHistory == "ASC") {
                viewModel.priceOrderBy.value = "ASC"
                radioAsc?.isChecked = true
            } else {
                viewModel.priceOrderBy.value = "DESC"
                radioDesc?.isChecked = true
            }
        }

        radioGroup?.setOnCheckedChangeListener { group, checkedId ->
            when (checkedId) {
                R.id.radio_asc -> {
                    viewModel.priceOrderBy.value = "ASC"
                }
                R.id.radio_desc -> {
                    viewModel.priceOrderBy.value = "DESC"
                }
            }
        }

        btnSet?.setOnClickListener {
            getProductLocal()
            dialog.dismiss()
        }

        dialog.show()
    }

}