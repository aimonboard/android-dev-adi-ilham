package com.aimonboard.twiscodeandroid.ui.cart

import android.os.Bundle
import android.view.*
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.recyclerview.widget.LinearLayoutManager
import com.aimonboard.twiscodeandroid.data.local.entity.CartEntity
import com.aimonboard.twiscodeandroid.databinding.FragmentCartBinding
import com.aimonboard.twiscodeandroid.ui.adapter.CartAdapter
import dagger.hilt.android.AndroidEntryPoint
import java.text.NumberFormat
import java.util.*
import kotlin.collections.ArrayList

@AndroidEntryPoint
class CartFragment : Fragment(), CartAdapter.ItemClickInterface {

    private lateinit var binding: FragmentCartBinding
    private lateinit var adapter: CartAdapter
    private val viewModel: CartViewModel by viewModels()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        setHasOptionsMenu(true)
        binding = FragmentCartBinding.inflate(inflater, container, false)
        binding.viewModel = viewModel
        binding.lifecycleOwner = this
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupAdapter()
        getProductLocal()
        total()
    }

    override fun plus(productId: String) {
        viewModel.plus(productId)
    }

    override fun minus(productId: String) {
        viewModel.minus(productId)
    }

    private fun setupAdapter() {
        adapter = CartAdapter(this)
        binding.recyclerCart.adapter = adapter
        binding.recyclerCart.layoutManager = LinearLayoutManager(requireContext())
    }

    private fun getProductLocal() {
        binding.shimmerLayout.startShimmer()
        binding.shimmerLayout.visibility = View.VISIBLE
        binding.recyclerCart.visibility = View.GONE

        viewModel.getCart().observe(viewLifecycleOwner, {
            binding.shimmerLayout.stopShimmer()
            binding.shimmerLayout.visibility = View.GONE
            binding.recyclerCart.visibility = View.VISIBLE

            adapter.setItems(it as ArrayList<CartEntity>)
        })
    }

    private fun total() {
        val localeID = Locale("in", "ID")
        val formatRupiah = NumberFormat.getCurrencyInstance(localeID)
        viewModel.total().observe(viewLifecycleOwner, {
            val priceString = "Rp ${formatRupiah.format(it).replace("Rp", "")}"
            binding.totalValue.text = priceString
        })
    }

}