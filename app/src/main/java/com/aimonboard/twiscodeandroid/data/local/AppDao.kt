package com.aimonboard.twiscodeandroid.data.local

import androidx.lifecycle.LiveData
import androidx.room.*
import androidx.sqlite.db.SimpleSQLiteQuery
import com.aimonboard.twiscodeandroid.data.local.entity.CartEntity
import com.aimonboard.twiscodeandroid.data.local.entity.CategoryEntity
import com.aimonboard.twiscodeandroid.data.local.entity.ProductEntity

@Dao
interface AppDao {

//    @Query("SELECT * FROM Product WHERE title LIKE '%' || :searchText || '%' AND categoryId LIKE '%' || :categoryId || '%' ORDER BY price || :priceOrderBy")
//    fun productGetAll(searchText: String, categoryId: String, priceOrderBy: String): LiveData<List<ProductEntity>>

    @RawQuery(observedEntities = [ProductEntity::class])
    fun productGetAll(query: SimpleSQLiteQuery): LiveData<List<ProductEntity>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun productInsert(productData: ArrayList<ProductEntity>)

    @Query("DELETE FROM Product WHERE id=:productId")
    fun productDelete(productId: String)



    @Query("SELECT * FROM cart")
    fun cartGetAll(): LiveData<List<CartEntity>>

    @Query("SELECT * FROM cart WHERE productId=:productId")
    fun cartGetByProductId(productId: String): List<CartEntity>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun cartInsert(cartEntity: CartEntity)

    @Query("UPDATE cart SET count=count+1 WHERE productId=:productId")
    fun cartPlus(productId: String)

    @Query("UPDATE cart SET count=count-1 WHERE productId=:productId")
    fun cartMinus(productId: String)

    @Query("SELECT COUNT(*) FROM cart")
    fun cartCount(): LiveData<Int>

    @Query("SELECT SUM(price * count) FROM cart")
    fun cartTotal(): LiveData<Double>



    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun categoryInsert(categoryData: ArrayList<CategoryEntity>)

    @Query("SELECT * FROM category")
    fun categoryGetAll(): LiveData<List<CategoryEntity>>

    @Query("UPDATE category set isChecked=:isChecked WHERE id=:categoryId")
    fun categoryUpdateChecked(categoryId: String, isChecked: Boolean)

    @Query("UPDATE category set isChecked=:isChecked")
    fun categoryUpdateCheckedAll(isChecked: Boolean)

}