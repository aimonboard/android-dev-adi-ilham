package com.aimonboard.twiscodeandroid.data.local

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.aimonboard.twiscodeandroid.data.local.entity.CartEntity
import com.aimonboard.twiscodeandroid.data.local.entity.CategoryEntity
import com.aimonboard.twiscodeandroid.data.local.entity.ProductEntity

@Database(
    entities = [
        ProductEntity::class,
        CartEntity::class,
        CategoryEntity::class
    ],
    version = 1,
    exportSchema = false
)

abstract class AppDatabase : RoomDatabase() {

    abstract fun dao(): AppDao

    companion object {
        @Volatile private var instance: AppDatabase? = null

        fun getDatabase(context: Context): AppDatabase =
            instance ?: synchronized(this) { instance ?: buildDatabase(context).also { instance = it } }

        private fun buildDatabase(appContext: Context) =
            Room.databaseBuilder(appContext, AppDatabase::class.java, "Database")
                .fallbackToDestructiveMigration()
                .build()
    }

}