package com.aimonboard.twiscodeandroid.data.local.entity

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "product")
data class ProductEntity (
    @PrimaryKey(autoGenerate = false)
    val id: String,
    val title: String,
    val price: Double,
    val city: String,
    val conditionOfItem: String,
    val categoryId: String,
    val userName: String,
    val weight: Double,
    val isSoldOut: Int,
    val isHalal: Int,
)