package com.aimonboard.twiscodeandroid.data.local.entity

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "cart")
data class CartEntity (
    @PrimaryKey(autoGenerate = true)
    val id: Int,
    val productId: String,
    val title: String,
    val price: Double,
    val city: String,
    val count: Int,
    val conditionOfItem: String,
    val userName: String,
    val weight: Double,
    val isSoldOut: Int,
    val isHalal: Int,
)