package com.aimonboard.twiscodeandroid.data.remote


import com.google.gson.annotations.SerializedName

class ProductResponse : ArrayList<ProductResponse.ListModelItem>(){
    data class ListModelItem(
        @SerializedName("added_date")
        val addedDate: String,
        @SerializedName("added_date_str")
        val addedDateStr: String,
        @SerializedName("added_user_id")
        val addedUserId: String,
        @SerializedName("added_user_name")
        val addedUserName: String,
        @SerializedName("address")
        val address: String,
        @SerializedName("appear_duration")
        val appearDuration: String,
        @SerializedName("brand")
        val brand: String,
        @SerializedName("business_mode")
        val businessMode: String,
        @SerializedName("cat_id")
        val catId: String,
        @SerializedName("category")
        val category: Category,
        @SerializedName("condition_of_item")
        val conditionOfItem: Any,
        @SerializedName("condition_of_item_id")
        val conditionOfItemId: String,
        @SerializedName("deal_option")
        val dealOption: DealOption,
        @SerializedName("deal_option_id")
        val dealOptionId: String,
        @SerializedName("deal_option_remark")
        val dealOptionRemark: String,
        @SerializedName("default_photo")
        val defaultPhoto: DefaultPhoto,
        @SerializedName("description")
        val description: String,
        @SerializedName("favourite_count")
        val favouriteCount: String,
        @SerializedName("get_address")
        val getAddress: Any,
        @SerializedName("highlight_info")
        val highlightInfo: String,
        @SerializedName("id")
        val id: String,
        @SerializedName("is_available")
        val isAvailable: String,
        @SerializedName("is_favourited")
        val isFavourited: String,
        @SerializedName("is_food")
        val isFood: String,
        @SerializedName("is_free")
        val isFree: String,
        @SerializedName("is_halal")
        val isHalal: String,
        @SerializedName("is_owner")
        val isOwner: String,
        @SerializedName("is_paid")
        val isPaid: String,
        @SerializedName("is_pre_order")
        val isPreOrder: String,
        @SerializedName("is_sold_out")
        val isSoldOut: String,
        @SerializedName("item_currency")
        val itemCurrency: ItemCurrency,
        @SerializedName("item_currency_id")
        val itemCurrencyId: String,
        @SerializedName("item_location")
        val itemLocation: ItemLocation,
        @SerializedName("item_location_id")
        val itemLocationId: String,
        @SerializedName("item_price_type")
        val itemPriceType: ItemPriceType,
        @SerializedName("item_price_type_id")
        val itemPriceTypeId: String,
        @SerializedName("item_type")
        val itemType: ItemType,
        @SerializedName("item_type_id")
        val itemTypeId: String,
        @SerializedName("lat")
        val lat: String,
        @SerializedName("lng")
        val lng: String,
        @SerializedName("location_id")
        val locationId: String,
        @SerializedName("location_name")
        val locationName: String,
        @SerializedName("location_type")
        val locationType: String,
        @SerializedName("paid_status")
        val paidStatus: String,
        @SerializedName("photo_count")
        val photoCount: String,
        @SerializedName("pickup_time")
        val pickupTime: String,
        @SerializedName("po_delivery")
        val poDelivery: String,
        @SerializedName("po_end")
        val poEnd: String,
        @SerializedName("po_slot")
        val poSlot: String,
        @SerializedName("po_start")
        val poStart: String,
        @SerializedName("price")
        val price: String,
        @SerializedName("status")
        val status: String,
        @SerializedName("stock")
        val stock: String,
        @SerializedName("sub_cat_id")
        val subCatId: String,
        @SerializedName("sub_category")
        val subCategory: SubCategory,
        @SerializedName("title")
        val title: String,
        @SerializedName("touch_count")
        val touchCount: String,
        @SerializedName("updated_date")
        val updatedDate: String,
        @SerializedName("updated_flag")
        val updatedFlag: String,
        @SerializedName("updated_user_id")
        val updatedUserId: String,
        @SerializedName("user")
        val user: User,
        @SerializedName("weight")
        val weight: String
    ) {
        data class Category(
            @SerializedName("added_date")
            val addedDate: String,
            @SerializedName("cat_id")
            val catId: String,
            @SerializedName("cat_name")
            val catName: String,
            @SerializedName("cat_ordering")
            val catOrdering: String,
            @SerializedName("default_icon")
            val defaultIcon: DefaultIcon,
            @SerializedName("default_photo")
            val defaultPhoto: DefaultPhoto,
            @SerializedName("is_food")
            val isFood: String,
            @SerializedName("status")
            val status: String
        ) {
            data class DefaultIcon(
                @SerializedName("img_desc")
                val imgDesc: String,
                @SerializedName("img_height")
                val imgHeight: String,
                @SerializedName("img_id")
                val imgId: String,
                @SerializedName("img_parent_id")
                val imgParentId: String,
                @SerializedName("img_path")
                val imgPath: String,
                @SerializedName("img_type")
                val imgType: String,
                @SerializedName("img_width")
                val imgWidth: String
            )
    
            data class DefaultPhoto(
                @SerializedName("img_desc")
                val imgDesc: String,
                @SerializedName("img_height")
                val imgHeight: String,
                @SerializedName("img_id")
                val imgId: String,
                @SerializedName("img_parent_id")
                val imgParentId: String,
                @SerializedName("img_path")
                val imgPath: String,
                @SerializedName("img_type")
                val imgType: String,
                @SerializedName("img_width")
                val imgWidth: String
            )
        }
    
        data class ConditionOfItem(
            @SerializedName("added_date")
            val addedDate: String,
            @SerializedName("id")
            val id: String,
            @SerializedName("name")
            val name: String,
            @SerializedName("status")
            val status: String
        )
    
        data class DealOption(
            @SerializedName("added_date")
            val addedDate: String,
            @SerializedName("id")
            val id: String,
            @SerializedName("name")
            val name: String,
            @SerializedName("status")
            val status: String
        )
    
        data class DefaultPhoto(
            @SerializedName("img_desc")
            val imgDesc: String,
            @SerializedName("img_height")
            val imgHeight: String,
            @SerializedName("img_id")
            val imgId: String,
            @SerializedName("img_parent_id")
            val imgParentId: String,
            @SerializedName("img_path")
            val imgPath: String,
            @SerializedName("img_type")
            val imgType: String,
            @SerializedName("img_width")
            val imgWidth: String
        )
    
        data class GetAddress(
            @SerializedName("bs_items_id")
            val bsItemsId: String,
            @SerializedName("city")
            val city: String,
            @SerializedName("city_id")
            val cityId: String,
            @SerializedName("contact_person")
            val contactPerson: String,
            @SerializedName("created_at")
            val createdAt: String,
            @SerializedName("deleted_at")
            val deletedAt: String,
            @SerializedName("district")
            val district: String,
            @SerializedName("district_id")
            val districtId: String,
            @SerializedName("full_address")
            val fullAddress: String,
            @SerializedName("id")
            val id: String,
            @SerializedName("latitude")
            val latitude: String,
            @SerializedName("longitude")
            val longitude: String,
            @SerializedName("name_address")
            val nameAddress: String,
            @SerializedName("province")
            val province: String,
            @SerializedName("province_id")
            val provinceId: String,
            @SerializedName("updated_at")
            val updatedAt: String,
            @SerializedName("zip_code")
            val zipCode: String
        )
    
        data class ItemCurrency(
            @SerializedName("added_date")
            val addedDate: String,
            @SerializedName("currency_short_form")
            val currencyShortForm: String,
            @SerializedName("currency_symbol")
            val currencySymbol: String,
            @SerializedName("id")
            val id: String,
            @SerializedName("is_empty_object")
            val isEmptyObject: String,
            @SerializedName("status")
            val status: String
        )
    
        data class ItemLocation(
            @SerializedName("added_date")
            val addedDate: String,
            @SerializedName("id")
            val id: String,
            @SerializedName("lat")
            val lat: String,
            @SerializedName("lng")
            val lng: String,
            @SerializedName("name")
            val name: String,
            @SerializedName("status")
            val status: String
        )
    
        data class ItemPriceType(
            @SerializedName("added_date")
            val addedDate: String,
            @SerializedName("id")
            val id: String,
            @SerializedName("is_empty_object")
            val isEmptyObject: String,
            @SerializedName("name")
            val name: String,
            @SerializedName("status")
            val status: String
        )
    
        data class ItemType(
            @SerializedName("added_date")
            val addedDate: String,
            @SerializedName("id")
            val id: String,
            @SerializedName("is_empty_object")
            val isEmptyObject: String,
            @SerializedName("name")
            val name: String,
            @SerializedName("status")
            val status: String
        )
    
        data class SubCategory(
            @SerializedName("added_date")
            val addedDate: String,
            @SerializedName("added_user_id")
            val addedUserId: String,
            @SerializedName("cat_id")
            val catId: String,
            @SerializedName("default_photo")
            val defaultPhoto: DefaultPhoto,
            @SerializedName("id")
            val id: String,
            @SerializedName("name")
            val name: String,
            @SerializedName("status")
            val status: String,
            @SerializedName("updated_date")
            val updatedDate: String,
            @SerializedName("updated_flag")
            val updatedFlag: String,
            @SerializedName("updated_user_id")
            val updatedUserId: String
        ) {
            data class DefaultPhoto(
                @SerializedName("img_desc")
                val imgDesc: String,
                @SerializedName("img_height")
                val imgHeight: String,
                @SerializedName("img_id")
                val imgId: String,
                @SerializedName("img_parent_id")
                val imgParentId: String,
                @SerializedName("img_path")
                val imgPath: String,
                @SerializedName("img_type")
                val imgType: String,
                @SerializedName("img_width")
                val imgWidth: String,
                @SerializedName("is_empty_object")
                val isEmptyObject: String
            )
        }
    
        data class User(
            @SerializedName("added_date")
            val addedDate: String,
            @SerializedName("apple_id")
            val appleId: String,
            @SerializedName("apple_verify")
            val appleVerify: String,
            @SerializedName("city")
            val city: String,
            @SerializedName("code")
            val code: String,
            @SerializedName("device_token")
            val deviceToken: String,
            @SerializedName("email_verify")
            val emailVerify: String,
            @SerializedName("facebook_id")
            val facebookId: String,
            @SerializedName("facebook_verify")
            val facebookVerify: String,
            @SerializedName("follower_count")
            val followerCount: String,
            @SerializedName("following_count")
            val followingCount: String,
            @SerializedName("google_id")
            val googleId: String,
            @SerializedName("google_verify")
            val googleVerify: String,
            @SerializedName("is_banned")
            val isBanned: String,
            @SerializedName("is_followed")
            val isFollowed: String,
            @SerializedName("messenger")
            val messenger: String,
            @SerializedName("overall_rating")
            val overallRating: String,
            @SerializedName("phone_id")
            val phoneId: String,
            @SerializedName("phone_verify")
            val phoneVerify: String,
            @SerializedName("rating_count")
            val ratingCount: String,
            @SerializedName("rating_details")
            val ratingDetails: RatingDetails,
            @SerializedName("role_id")
            val roleId: String,
            @SerializedName("status")
            val status: String,
            @SerializedName("user_about_me")
            val userAboutMe: String,
            @SerializedName("user_address")
            val userAddress: String,
            @SerializedName("user_cover_photo")
            val userCoverPhoto: String,
            @SerializedName("user_email")
            val userEmail: String,
            @SerializedName("user_id")
            val userId: String,
            @SerializedName("user_is_sys_admin")
            val userIsSysAdmin: String,
            @SerializedName("user_name")
            val userName: String,
            @SerializedName("user_phone")
            val userPhone: String,
            @SerializedName("user_profile_photo")
            val userProfilePhoto: String,
            @SerializedName("whatsapp")
            val whatsapp: String
        ) {
            data class RatingDetails(
                @SerializedName("five_star_count")
                val fiveStarCount: String,
                @SerializedName("five_star_percent")
                val fiveStarPercent: String,
                @SerializedName("four_star_count")
                val fourStarCount: String,
                @SerializedName("four_star_percent")
                val fourStarPercent: String,
                @SerializedName("one_star_count")
                val oneStarCount: String,
                @SerializedName("one_star_percent")
                val oneStarPercent: String,
                @SerializedName("three_star_count")
                val threeStarCount: String,
                @SerializedName("three_star_percent")
                val threeStarPercent: String,
                @SerializedName("total_rating_count")
                val totalRatingCount: String,
                @SerializedName("total_rating_value")
                val totalRatingValue: String,
                @SerializedName("two_star_count")
                val twoStarCount: String,
                @SerializedName("two_star_percent")
                val twoStarPercent: String
            )
        }
    }
}