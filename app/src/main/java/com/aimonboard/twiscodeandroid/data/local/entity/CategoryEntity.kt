package com.aimonboard.twiscodeandroid.data.local.entity

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "category")
data class CategoryEntity (
    @PrimaryKey(autoGenerate = false)
    val id: String,
    val name: String,
    val isChecked: Boolean
)