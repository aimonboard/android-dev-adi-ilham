## Feature

1. Navigation Component
2. SQlite + Room Database
3. Retrofit 2
4. Dagger Hilt
5. Coroutine
6. MVVM

## Screen Shoot

![Home](https://gitlab.com/aimonboard/android-dev-adi-ilham/-/blob/master/Picture/Home.png?raw=true)

![Search](https://gitlab.com/aimonboard/android-dev-adi-ilham/-/blob/master/Picture/Search.png?raw=true)

![Category](https://gitlab.com/aimonboard/android-dev-adi-ilham/-/blob/master/Picture/Category.png?raw=true)

![Filter](https://gitlab.com/aimonboard/android-dev-adi-ilham/-/blob/master/Picture/Filter.png?raw=true)

![Cart](https://gitlab.com/aimonboard/android-dev-adi-ilham/-/blob/master/Picture/Cart.png?raw=true)
